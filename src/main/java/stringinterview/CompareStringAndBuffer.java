package stringinterview;

import org.testng.annotations.Test;

public class CompareStringAndBuffer {
	@Test
	public void compareStringAndStringBuffer() {
		String a="a";
		String b="a";
		String c= new String("a");
		String d= new String("a");
		StringBuilder sb = new StringBuilder("a");
		StringBuilder sb1 = new StringBuilder("a");
		if(a.equals(b))
			System.out.println("a is equal to b");   //a.equals(b)?a:b;
		else
			System.out.println("a is not equal to b");
		if(a == b)
			System.out.println("a is equal to b");
		else
			System.out.println("a is not equal to b");
		if(c == d)
			System.out.println("a is equal to b");
		else
			System.out.println("a is not equal to b");
		if(c.equals(d))
			System.out.println("a is equal to b");
		else
			System.out.println("a is not equal to b");
		if(a.equals(c))
			System.out.println("a is equal to b");
		else
			System.out.println("a is not equal to b");
		if(sb.equals(sb1))
			System.out.println("a is equal to b");
		else
			System.out.println("a is not equal to b");
		if(sb == sb1)
			System.out.println("a is equal to b");
		else
			System.out.println("a is not equal to b");
		/*
		 *we cannot compare string and string builder directly, 
		 *we have to covert string builder object to string 
		*/  
		if(sb.toString().equals(c))
			System.out.println("a is equal to b");
		else
			System.out.println("a is not equal to b");
	}
}
