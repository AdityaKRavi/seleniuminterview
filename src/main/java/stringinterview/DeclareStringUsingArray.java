package stringinterview;

import org.testng.annotations.Test;

public class DeclareStringUsingArray {
	@Test
	public void declareString() {
		final int max=10;
		String [] arrayOfStrings1 = new String [max];
		arrayOfStrings1[0] ="Aditya";
		arrayOfStrings1[1] ="Ankit";
		arrayOfStrings1[2] ="Suresh";
		arrayOfStrings1[3] ="Gowtham";
		arrayOfStrings1[4] ="Balaji";
		arrayOfStrings1[5] ="Salman";
		arrayOfStrings1[6] ="Amit";
		
		for (String string : arrayOfStrings1) {
			System.out.println(string);
		}
		
		String [] arrayOfStrings = {"Aditya", "Amit", "Akshay" , "Suresh", "Salman", "Balaji", "Gowtham", "Latcham"};
		for (String string : arrayOfStrings) {
			System.out.println(string);
		}
		
	}
}
