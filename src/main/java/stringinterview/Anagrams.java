package stringinterview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


public class Anagrams {

	public static void main(String[] args) {

		/*
		 * ******************************Logic***************************
		 * take two strings as input
		 * remove all the spaces if applicable from both the strings(can use Character wrapper class to remove whitespace or write logic)
		 * sort both the strings(can use toCharArray, list of characters)
		 * compare both the strings(for loop)
		 */
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the first string: ");
		String firstString = scan.nextLine();
		System.out.println("Enter the second string: ");
		String secondString = scan.nextLine();
		List<Character> list1 = removeSpaceAndSort(firstString);
		List<Character> list2 = removeSpaceAndSort(secondString);
		if(compareLists(list1, list2))
			System.out.println("The entered strings are anagram");
		else
			System.out.println("The entered strings are not anagram");
		scan.close();
	}
	public static List<Character> removeSpaceAndSort(String passString) {
		char[] charArray = passString.toCharArray();
		List<Character> newlist = new ArrayList<>();
		for(int i=0;i<charArray.length;i++) {
			if(charArray[i]!=' ')
				newlist.add(charArray[i]);			
		}
		Collections.sort(newlist);
		return newlist;
	}
	
	public static boolean compareLists(List list1, List list2) {
		boolean isAnagram=false;
		if(list1.size() == list2.size()) {
			for(int i=0; i<list1.size(); i++)
				if(list1.get(i).equals(list2.get(i)))
					isAnagram=true;
				else 
					isAnagram=false;
		}else {
			System.out.println("The entered strings are of different size...Abort");
			System.exit(0);
		}
			return isAnagram;
	}
}







