package selenium;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ActionsClass {
	
	RemoteWebDriver driver = null;
	
	@BeforeMethod
	public void setUp() {
		ChromeOptions op = new ChromeOptions();
		op.setHeadless(false);
		driver = new ChromeDriver(op);
		driver.manage().window().maximize();
		driver.get("http://www.google.com");

	}
	
	@Test
	public void selectSelectedOptions() {
		
		Actions builder =new Actions(driver);
		WebElement goobutton = driver.findElementByName("btnK");
		builder.contextClick(goobutton).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).build().perform();
	}
	@AfterMethod
	public void tearDown() {
//		driver.close();
	}
}


