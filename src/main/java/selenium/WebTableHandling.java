/*********Find the row number and column number of the data entered through console***********/


package selenium;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



public class WebTableHandling {
	public RemoteWebDriver driver ;

	@BeforeTest
	public void setUp() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.w3schools.com/html/html_tables.asp");	
	}

	public void webTable(WebElement table, int rowNumber, int columnNumber) {
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> cols = rows.get(rowNumber).findElements(By.tagName("td"));
		System.out.println(cols.get(columnNumber).getText());
	}
	//table[@id='customers']/tbody//th[text()='Country']//following::tr/td[3]
	public List<String> getDataByColumnName(RemoteWebDriver driver,String columnName) {
		Map<String,Integer> columnDetails = new LinkedHashMap<>();
		WebElement webTable = driver.findElementByXPath("//table[@id='customers']");
		List<WebElement> allRows = webTable.findElements(By.tagName("tr"));
		List<WebElement> columnHead = allRows.get(0).findElements(By.tagName("th"));
		for(int i=0;i<columnHead.size();i++) {
			columnDetails.put(columnName, i);
		}
		List<WebElement> column = driver.findElements(By.xpath("//table[@id='customers']/tbody//th[text()='"+columnName+"']//following::tr/td["+columnDetails.get(columnName)+"]"));
		List<String> columnData = new ArrayList<String>();
		for (WebElement Column : column) {
			columnData.add(Column.getText());
		}
		return columnData;
	}
	
	
	/*@Test
	public void webTableHandle() {
		WebTableHandling wt= new WebTableHandling();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the row number and column number");
		int row = scan.nextInt();
		int column = scan.nextInt();
		WebElement table = driver.findElementByXPath("//table[@id='customers']");
		if(row>7 && column>2 && row<0 && column<0) {
			System.out.println("Please enter valid row and column number");
		}
		else {
			try {
				wt.webTable(table, row, column-1);
			}catch(ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		}
		scan.close();
	}*/
	
	@Test
	public void webTableHandles() {
		WebTableHandling wt= new WebTableHandling();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the column name for which you want the data");
		System.out.println("The columns are: /n1.Company /n2.Contact /n3.Country ");
		String input = scan.next();
		List<String> dataByColumnName = wt.getDataByColumnName(driver, input);
		for (String string : dataByColumnName) {
			System.out.println(string);
		}
		scan.close();
	}


	/*@Test
	public void webTableHandling() {
		WebElement webTable = driver.findElementByXPath("//table[@id='customers']");
		List<WebElement> allRows = webTable.findElements(By.tagName("tr"));
		System.out.println(allRows.size());
		for (WebElement eachRow : allRows) {
			List<WebElement> allCols = eachRow.findElements(By.tagName("td"));
			for (WebElement eachCol : allCols) {
					System.out.println("Table Data");
					System.out.println(eachCol.getText());
			}
		}
	}*/

	@AfterTest
	public void tearDown() {
		driver.close();
	}








}
