package seleniuminterview;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class InterviewSeries4Question1{
	
	RemoteWebDriver driver = null;
	@BeforeMethod
	public void setUp() {
		ChromeOptions op = new ChromeOptions();
		op.setHeadless(true);
		driver = new ChromeDriver(op);
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
	}
	
	@Test
	public void selectSelectedOptions() {
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_generalCountryGeoId");
		WebElement selectCountry = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select select = new Select(selectCountry);
		List<WebElement> options = select.getOptions();
		options.size();
		for (WebElement webElement : options) {
			if(webElement.getText().startsWith("E")) {
				int indexOf = options.indexOf(webElement);
				select.selectByIndex(indexOf+1);
				System.out.println(select.getFirstSelectedOption().getText());
				break;
			}
		}
	}
	@AfterMethod
	public void tearDown() {
		driver.close();
	}
}
