package seleniuminterview;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FinallyDoesntWorkCase {

	RemoteWebDriver driver = null;
	@BeforeMethod
	public void setUp() {
		ChromeOptions op = new ChromeOptions();
		op.setHeadless(true);
		driver = new ChromeDriver(op);
		driver.manage().window().maximize();
	}
		
	@Test(description="Testing the finally block")
	public void testFinally() {	
		try {
		driver.get("https://www.google.com/");
		driver.findElementByXPath("//input[@type='text']").sendKeys("Selenium");;
		driver.findElementByName("btnK").sendKeys(Keys.ENTER);
		System.exit(0);
		}catch(NoSuchElementException e) {
			System.out.println("In the catch block");
		}
		finally {
			System.out.println("In the finally block");
		}
	}	
}
