package seleniuminterview;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class MoveControlToLastWindow {
	RemoteWebDriver driver = null;
	@Test(description="Navigate to Windows Test")
	public void moveControlToLastWindow() {
		ChromeOptions op = new ChromeOptions();
		op.setHeadless(false);
		op.addArguments("--start-maximized");
		op.addArguments("--test-name");
		driver = new ChromeDriver(op);
//		driver.manage().window().maximize();
		driver.get("https://www.indeed.co.in/Fresher-jobs");

		List<WebElement> jobLinks = driver.findElementsByXPath("((//td[@id='resultsCol'])//a[@data-tn-element='jobTitle'])");	
		List<String> window = new ArrayList<String>();
		int size = jobLinks.size();
		Set<String> allWindows = new LinkedHashSet<>();
		for(int i =0; i<size; i++) {
			Actions builder = new Actions(driver);
			builder.keyDown(Keys.CONTROL).click(jobLinks.get(i)).keyUp(Keys.CONTROL).perform();
			allWindows = driver.getWindowHandles();
		}	
		window.addAll(allWindows);
		driver.switchTo().window(window.get(size));
		System.out.println(driver.getTitle());
	}
}
