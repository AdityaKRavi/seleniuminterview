package seleniuminterview;

public class ConstructorAndDestructor {

	int age;
	String Name;

	/*Constructor is a block of code that initializes the 
	newly created object. A constructor resembles an instance 
	method in java but it�s not a method as it doesn�t have a return type.*/
	//Even if we don't define a constructor the JVM provides a default  
	public ConstructorAndDestructor(int age, String name) {
		age = this.age;
		name= this.Name;
	}

	public static void main(String[] args) {
		ConstructorAndDestructor cnd = new ConstructorAndDestructor(26, "Aditya");
		//Here a new Object is created and assigned the values of age and name.
		/*If no parameters are passed in the constructor default values are assigned to the members,
	  i.e integer values will be 0, String will be null, Double will be 0.0 etc*/
		System.out.println("Name :" +cnd.Name+  "Age :" +cnd.age);	
	}
	/*There is no concept of destructor in java (unlike c/c++). Java GC(Garbage collector) makes use of 
	 *finalize method to clear resources no more required by objects 
	 */
}

