package seleniuminterview;

public class NoClassDefinition {
	
	/*
	 *1. Run the code
	 *2. Right Click on the project folder--> show in -->System Explorer
	 *3. Navigate to folder bin of the project--> go to the package in which u created the class
	 *4. Delete the .class file of this class you compiled
	 *5. Run this code once again
	 *6. You will get error
	 * 
	 */
	
	public static void main(String[] args) {
		System.out.println("Hello World");
	}
}
