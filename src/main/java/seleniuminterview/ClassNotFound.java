package seleniuminterview;

import java.lang.reflect.Field;

public class ClassNotFound{

	
	public static void main(String[] args) throws ClassNotFoundException {
		
//		CreateObj ob = new CreateObj();
		System.out.println("Hello World");
		Class<?> cls = Class.forName("seleniuminterview.ConstructorAndDestructor");
		String classname = cls.getName();
		System.out.println("class name is: "+classname);
		Field[] fields = cls.getDeclaredFields();
		for (Field field : fields) {
			System.out.println("All member variables : "+field);
		}
	}
}
