package seleniuminterview;

public class FinallyDoesntWork2 {

	public double divide(int num1,int num2) {
		
		double result=1;
		try {
			result = num1/num2;
			return result;
		} catch (Exception e) {
			System.out.println("In the catch block");
			throw new RuntimeException();
		}finally {
			System.out.println("In the Finally block");
		}
	}
	
	public static void main(String[] args) {
		FinallyDoesntWork2 obj = new FinallyDoesntWork2();
		double result = obj.divide(20,0);
		System.out.println(result);		
	}
}
