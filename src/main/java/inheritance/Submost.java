package inheritance;

public class Submost extends SuperSub{

	public void Hello(){
		System.out.println("Hello Submost");
	}
	public void Hi() {
		super.Hello();
	}
	public static void main(String[] args) {
		Submost sb =new Submost();
		sb.Hi();
	}
}
