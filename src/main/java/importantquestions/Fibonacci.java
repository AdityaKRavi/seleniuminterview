package importantquestions;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
	public static void main(String[] args) {
//		int first =1;
//		int second=1;
//		int sum =0;
//		while(first<=34) {
//			sum = first+second;
//			System.out.println(first);
//			first = second;
//			second = sum;
//		}
		//2.Method
		List<Integer> fibonacci = new ArrayList<>();
		fibonacci.add(1);
		fibonacci.add(1);
		for(int i=0; i<7;i++) {
			fibonacci.add(i+2, (fibonacci.get(i)+fibonacci.get(i+1)));
		}
		for (Integer integer : fibonacci) {
			System.out.println(integer);
		}	
	}
}
