package importantquestions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;

import com.google.common.collect.Streams;

public class MaxAndMin {

	public static void main(String[] args) {
		int [] numbers = {11,13,3,7,18,8,9};
		//1.Method
		/*List<Integer> list = new ArrayList<Integer>();
		for(int i=0;i<numbers.length;i++)
			list.add(numbers[i]);
		System.out.println(Collections.max(list));*/	
		//2.Method
		IntStream stream = Arrays.stream(numbers);
		System.out.println(stream.max().getAsInt());
		//3. Method
		/*		Arrays.sort(numbers);
		System.out.println(numbers[numbers.length-1]);*/
		//4. Method
	}
}
