package importantquestions;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DuplicateNumbers {

	public static void main(String[] args) {
		//1st Method
		int [] duplicate = {1,3,3,7,8,8,9};
		IntStream distinct = Arrays.stream(duplicate).distinct();
		int[] array = distinct.toArray();
		for (int i : array) {
			System.out.println(i);
		}
		
		/*Set<Integer> duplicates = new LinkedHashSet<>();
		for(int i=0;i<duplicate.length;i++)
			duplicates.add(duplicate[i]);
		for (Integer integer : duplicates) 
			System.out.print(" "+integer);	
		*/
		//2nd Method
		/*int i,j;
		for(i=0,j=1;j<(duplicate.length);i++,j++) {
			if(duplicate[i]==duplicate[j])
				System.out.println(duplicate[i]);	
		}*/
	}
}
