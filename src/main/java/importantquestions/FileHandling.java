package importantquestions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileHandling {

	public static void main(String[] args) throws IOException {
		try {
			File file =new File("./data/data.txt");
			FileInputStream fis = new FileInputStream(file);
			if(file.exists()) {
					int available = fis.available();
					
					System.out.println((char)available);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
